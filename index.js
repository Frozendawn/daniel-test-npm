const packageJSON = require('./package.json');


/**
 * Returns the version of the package
 * @example
 * returns 1.0.0
 */
module.exports = function () {
  return packageJSON.version;
}