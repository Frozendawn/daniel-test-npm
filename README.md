# Publish to your npm package through a pipeline.
This repo showcases a easy way of runnning a publish job that will commit the current code in the main branch to your npm package.
Here is what you will need.
<ol>
  <li>Create npm account</li>
  <li>run `npm login`</li>
  <li>run `npm publish` to publish the first version of the package.</li>
  <li>Create access token with read/write permissions from the settings menu in npm.</li>
  <li>Change the version of the package.json eg. from 1.0.0 to 1.0.1 (since you can't publish to the same version)</li>
  <li>Go to gitlab settings and select CI/CD, create new variable and uncheck the protected variable checkbox and check the mask variable one. Give it a name of your choice(eg. `CI_JOB_TOKEN`) and paste the value from npm token you created in step 4</li>
  <li>If you didn't name your gitlab CI/CD token `CI_JOB_TOKEN` go to .gitlab-ci.yml and add you token name there</li>
  <li>Push a new commit to the main branch and go to the pipeline section of gitlab.</li>
</ol>